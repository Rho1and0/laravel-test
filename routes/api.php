<?php

//Route::group(['prefix' => 'api', 'before' => 'jwt.auth', 'after' => 'jwt.refresh'], function ($api) {
//Route::group(['prefix' => 'api'], function ($api) {

Route::get('usuarios', 'UsersController@listar');
Route::get('usuarios/visualizar/{id}', 'UsersController@visualizar');
Route::get('usuarios/print', 'UsersController@print_data');
Route::post('usuarios/filtrar', 'UsersController@filtrar');
Route::post('usuarios/almacenar', 'UsersController@almacenar');
Route::put('usuarios/actualizar/{id}', 'UsersController@actualizar');
Route::delete('usuarios/eliminar/{id}', 'UsersController@eliminar');
//});


Route::get('import_xls', 'XLSmanager@import');