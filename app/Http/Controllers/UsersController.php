<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Validator;
use Route;
use JWTAuth;
use App\User;
use mikehaertl\wkhtmlto\Pdf;

use App\Http\Requests;

class UsersController extends CruController
{
    private $model;
    private $controller;
    public function __construct()
    {
        $this->model = new User;
        $this->controller = new CruController($this->model);
        $this->middleware('jwt.auth', ['except' => ['listar', 'visualizar', 'tours',
            'almacenar', 'actualizar', 'print_data']]);
        //]]);
    }

    public function print_data() {
        $pdf = new Pdf(array(
            'disable-smart-shrinking',
            'user-style-sheet' => 'http://rho1and0.net/modules/custom/rho_mod/libraries/bootstrap/css/bootstrap.min.css,http://rho1and0.net/modules/custom/rho_mod/css/mockup.css',
        ));

        $pdf->addPage('http://www.rho1and0.net');

        if (!$pdf->send('C:\Users\INDOMITA-TECNOLOGIA\Desktop\proyects\laravel-test\public\report.pdf')) {
            $data = $pdf->getError();
            $data2 = '';//$pdf->getMessage();
        }
        return response()->json([
            'res' => $data,
            'message' => $data2,
            'data' => $pdf->toString(),
            'resultado' => 'Imprimio']);
    }

    //wkhtmltopdf.exe "http://jayrleo.com/cies/#/momento1" web.pdf

    public function listar() {
        return $this->controller->listar();
    }

    public function visualizar(Request $request, $id) {
        return $this->controller->visualizar($request, $id);
    }

    public function almacenar(Request $request) {
        $input = $request->all();

        $error = $this->validateData($input);

        if(!empty($error)) {
            return response()->json([
                'respuesta' => false,
                'errors' => $error]);
        }

        $this->encryptField($input);
        $request->replace($input);
        return $this->controller->almacenar($request);
    }

    public function actualizar(Request $request, $id) {
        $input = $request->all();

        $error = $this->validateData($input);

        if(!empty($error)) {
            return response()->json([
                'respuesta' => false,
                'errors' => $error]);
        }

        $this->encryptField($input);
        $request->replace($input);
        return $this->controller->actualizar($request, $id);
    }

    private function encryptField(Array &$data, $field_name='password') {
        if(!empty($data[$field_name])) {
            $data[$field_name] = bcrypt($data[$field_name]);
        }
        else {
            unset($data[$field_name]);
        }
    }

    private function validateData(Array &$data) {
        $error = array();

        $validator = Validator::make($data, [
            'alias' => 'required',
            'password' => 'required',
            'email' => 'required',
            'fullname' => 'required',
        ]);

        if($validator->fails()) {
            $error = $validator->errors();
        }

        return $error;
    }

    public function tours(Request $request, $v_bk) {
        $error = array();

        $tour = DB::table('tours')->where('id', $v_bk)->first();

        return view('tours', ['tour' => $tour]);
    }
}