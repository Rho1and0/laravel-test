<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class RolesController extends CrudController
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['listar', 'resetpassword', 'checkreset', 'newpassword', 'contactar']]);
    }
}