<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use App\Http\Requests;



class CruController extends Controller
{
    private $model;

    public function __construct($mod)
    {
        $this->model = $mod;
    }

    /**
     * @api {get} /tasks List all tasks
     * @apiGroup Tasks
     * @apiSuccess {Object[]} tasks Task's list
     * @apiSuccess {Number} tasks.id Task id
     * @apiSuccess {String} tasks.title Task title
     * @apiSuccess {Boolean} tasks.done Task is done?
     * @apiSuccess {Date} tasks.updated_at Update's date
     * @apiSuccess {Date} tasks.created_at Register's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [{
     *      "id": 1,
     *      "title": "Study",
     *      "done": false
     *      "updated_at": "2016-02-10T15:46:51.778Z",
     *      "created_at": "2016-02-10T15:46:51.778Z"
     *    }]
     * @apiErrorExample {json} List error
     *    HTTP/1.1 500 Internal Server Error
     */
    public function listar() {
        try {
            $queryresult = $this->model->all();
            return response()->json([
                'respuesta' => true,
                'resultado' => $queryresult]);
        } catch (QueryException $e) {
            return response()->json([
                'respuesta' => false,
                'resultado' => $e->errorInfo]);
        } catch (PDOException $e) {
            return response()->json([
                'respuesta' => false,
                'resultado' => 'No se pudo conectar con la base de datos.']);
        }
    }

    public function visualizar(Request $request, $id) {
        try {
            $queryresult = $this->model->find($id);

            return response()->json([
                'respuesta' => true,
                'resultado' => $queryresult]);

        } catch (QueryException $e) {
            return response()->json([
                'respuesta' => false,
                'resultado' => $e->errorInfo]);
        }

    }

    public function almacenar(Request $request) {
        try {
            $input = $request->all();

            $result = $this->model->create($input);

            return response()->json([
                'respuesta' => true,
                'resultado' => $result->id]);

        } catch (QueryException $e) {
            return response()->json([
                'respuesta' => false,
                'resultado' => $e->errorInfo]);
        }
    }

    public function actualizar(Request $request, $id) {
        try {
            $input = $request->all();

            $this->model->find($id)->update($input);

            return response()->json([
                'respuesta' => true]);

        } catch (QueryException $e) {
            return response()->json([
                'respuesta' => false,
                'resultado' => $e->errorInfo]);
        }
    }
}