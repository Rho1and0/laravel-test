<?php
/**
 * Created by PhpStorm.
 * User: INDOMITA-TECNOLOGIA
 * Date: 27/10/2017
 * Time: 14:42
 */

namespace App\Http\Controllers;

use Excel;


class XLSmanager {
    public function import() {

        $response = [];

        Excel::load('..\\Cuadro_indicadores.xlsx', function(LaravelExcelReader $reader) {
            // ->all() is a wrapper for ->get() and will work the same
            $results = $reader->get();
            $response = $reader;

            foreach($results as $sheet) {
                $sheetTitle = $sheet->getTitle();


                if($sheetTitle === '1') {
                    foreach($sheet as $row) {
                        //$response[] = $row;
                    }
                }
            }
        });

        return response()->json($response);
    }
}