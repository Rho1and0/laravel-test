<?php

use Styde\Seeder\BaseSeeder;

class UsersTableSeeder extends BaseSeeder
{

    protected $total = 5;

    public function getModel()
    {
        return new User();
    }

    public function getDummyData(Generator $faker, array $custom = [])
    {
        return [
            'name' => $faker->name,
            'email' => $faker->email,
            'password'  => bcrypt('123abc'),
            'alias' => $faker->alias,
            'fullname' => $faker->fullname,
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void

    public function run()
    {
    DB::table('users')->delete();
    $regs = array(
    [
    'id' => 1,
    'email' => 'test@test.com',
    'password' => Hash::make('abc123'),
    'alias' => 'Rho1and0',
    'fullname' => 'Rolando Monzon'
    ],
    );
    foreach ($regs as $reg) {
    DB::table('users')->insert($reg);
    }

    DB::table('users')
    ->where('id', 1)
    ->update(['alias' => 'Rho1and1', 'fullname' => 'Rolando Monzón']);
    } */
}