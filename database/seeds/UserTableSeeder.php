<?php

use Styde\Seeder\Seeder;
use Faker\Generator;
use App\User;

class UserTableSeeder extends Seeder
{
    protected $total = 10;

    public function getModel()
    {
        return new User();
    }

    public function getDummyData(Generator $faker, array $customValues = array())
    {
        $firstname = $faker->firstName;
        return [
            'email' => $faker->email,
            'password'  => bcrypt('123abc'),
            'alias' => $firstname,
            'fullname' => $firstname.' '.$faker->lastName
        ];
    }
}